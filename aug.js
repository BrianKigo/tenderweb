const express = require('express')
const app = express()
const port = 3002

//db
var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'arapseger',
    database: 'augDemo'
})

connection.connect()

//endpoints

app.get('/tenders', (req, res) => {
    connection.query('SELECT * from augDemo',
        (error, results, fields) => {
            if (error) {
                console.log('---err---', error);
            }
            res.send(results);
        });
})
app.post('/tenders', (req, res) => {
    console.log('New here', req.body);
    try {
        connection.query('INSERT INTO augDemo SET ?', req.body,
            (error, results, fields) => {
                console.log('error---', error);
                if (results) {
                    let tender = req.body;
                    tender.id = results.insertId;
                    console.log('results', results);
                    res.json(tender);
                } else {
                    res.json([]);
                }

            })
    } catch (err) {
        res.send(err)
    }
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))